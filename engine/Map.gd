extends Node3D

const config_buildings_path = "res://config/structures.json"
const map_size = 10
const map_scale = 0.51
const near_hex_even = [Vector2i(1,0),Vector2i(0,1),Vector2i(-1,1),Vector2i(-1,0),Vector2i(-1,-1),Vector2i(0,-1)]
const near_hex_odd = [Vector2i(1,0),Vector2i(1,1),Vector2i(0,1),Vector2i(-1,0),Vector2i(1,-1),Vector2i(0,-1)]
@onready var hex_plain = preload("res://objects/HexPlain.tscn")
var village = preload("res://objects/Village.tscn")
var capital = preload("res://objects/Capital.tscn")
var buildable = {"village":["green"],"capital":["green"]}

var map_array


func config_init():
	var config_buildings = FileAccess.get_file_as_string(config_buildings_path)
	var tmp = JSON.parse_string(config_buildings)
	print(tmp["village"])


func _init():
	config_init()


func _ready():
	map_array = []
	for i in range (0, map_size, 1):
		map_array.append([])
		for j in range (0, map_size, 1):
			map_array[i].append(null)
			var new_hex = create_hex(Vector2i(j,i))
			map_array[i][j] = new_hex


func change_hex(hex_, hex_type_str_):
	hex_.change_type(hex_type_str_)


func create_hex(pos_vector_):
	var buf = hex_plain.instantiate()
	var x = pos_vector_.x
	var y = pos_vector_.y
	add_child(buf)
	if (y+1)%2==0:
		buf.position = Vector3(map_scale*(x*1.73+0.866),0,y*map_scale*1.5)
	else:
		buf.position = Vector3(x*map_scale*1.73,0,y*map_scale*1.5)
	buf.change_coords(Vector2i(x,y))
	return buf


func get_near_hex(hex_):
	var temp_arr = []
	var near_hex
	near_hex = near_hex_odd if (hex_.coords.y+1)%2==0 else near_hex_even
	for i in near_hex:
		var temp_vec = i + Vector2i(hex_.coords.x,hex_.coords.y)
		if temp_vec.x < map_size and temp_vec.y < map_size and temp_vec.x >= 0 and temp_vec.y >= 0:
			temp_arr.append(map_array[temp_vec.y][temp_vec.x])
	return temp_arr


func settle(building_, hex_):
	if buildable[building_].has(hex_.type) and not hex_.has(building_):
		var temp = self.get(building_).instantiate()
		temp.position = hex_.position
		temp.position.y += 0.05
		add_child(temp)
		hex_.add_on_hex(temp)
		return temp
	else:
		return null


func create(object_, hex_):
	object_.position = Vector3(hex_.position.x,object_.position.y+0.1,hex_.position.z)
	hex_.add_on_hex(object_)
	add_child(object_)


func fight(attacker_,defender_):
	attacker_.position = defender_.position
	defender_.queue_free()


func get_hex(hexpos):
	return # hex instance


func is_walkable(hexpos) -> bool:
	"""
	Gets hex positions and
	returns is it walkable.
	"""
	return false


func get_walkable(hexpos):
	"""
	Gets entity hex position and
	returns array of possible walkable hexes.
	"""
	return [] # array of hexposes


func move_entity(entity, hexpos):
	"""
	Moves target entity to
	hex with hexpos.
	"""
	return
	
