extends Node


const turn_order = ["god", "king", "pawn"]
var current_turn

var pawn
var king
var god

func _ready():
	current_turn = turn_order.front()


func _process(delta):
	if god:
		check(current_turn)


func check(str_):
	var buf = self.get(str_)
	if buf.finished_turn:
		buf.finish_turn()
		change_turn()


func change_turn():
	var current_index = turn_order.find(current_turn)
	current_turn = turn_order.front() if current_index+1 == turn_order.size() else turn_order[current_index+1]


func _on_turn_timer_timeout():
	change_turn()
