extends Node3D


# Сделать по-человечески.
var camera
var gamestate
@onready var map = preload("res://engine/Map.tscn").instantiate()
var pawn
var king
var god


func _ready():
	camera = $Camera
	gamestate = $GameState
	add_child(map)
	god = preload("res://characters/God.tscn").instantiate()
	add_child(god)
	king = preload("res://characters/King.tscn").instantiate()
	add_child(king)
	pawn = preload("res://objects/Pawn.tscn").instantiate()
	add_child(pawn)
	pawn.position += Vector3(0,0.1,0)
	
	gamestate.god = god
	gamestate.king = king
	gamestate.pawn = pawn


func _process(delta):
	move_cam(delta)
	input_read()
	$Camera/Label.text = gamestate.current_turn


func input_read():
	if map and pick_object():
		if Input.is_action_just_pressed("click"):
			for i in map.get_near_hex(pick_object()):
				map.change_hex(i,"green")
		
	if Input.is_action_just_released("scroll_up"):
		print(camera.rotation)
		camera.position += Vector3(0.0,0.0,-1.0).rotated(Vector3(1.0,0.0,0.0),camera.rotation.x)
	if Input.is_action_just_released("scroll_down"):
		print(camera.rotation)
		camera.position -= Vector3(0.0,0.0,-1.0).rotated(Vector3(1.0,0.0,0.0),camera.rotation.x)


func move_cam(delta_):
	# Добавить перемещение на WSAD
	delta_ *= 5
	var viewport = get_viewport()
	var x_max = viewport.size.x
	var y_max = viewport.size.y
	var mouse_pos = viewport.get_mouse_position()
	
	if (mouse_pos.x < 5):
		camera.position.x -= delta_
	
	if (mouse_pos.x > x_max - 5):
		camera.position.x += delta_
	
	if (mouse_pos.y < 5):
		camera.position.z -= delta_
	
	if (mouse_pos.y > y_max - 5):
		camera.position.z += delta_


func pick_object():
	var mouse_pos = get_viewport().get_mouse_position()
	var from = camera.project_ray_origin(mouse_pos)
	var to = from + camera.project_ray_normal(mouse_pos) * 1000.0
	$RayCast.position = from
	$RayCast.target_position = to
	return($RayCast.get_collider())


func _input(event):
	if event is InputEventKey and event.is_pressed() and event.keycode == KEY_1:
		gamestate.current_turn = "pawn"
	elif event is InputEventKey and event.is_pressed() and event.keycode == KEY_2:
		gamestate.current_turn = "king"
	elif event is InputEventKey and event.is_pressed() and event.keycode == KEY_3:
		gamestate.current_turn = "god"
