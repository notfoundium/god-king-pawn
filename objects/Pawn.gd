extends CSGMesh3D

var moves_per_turn = 3

var finished_turn
var walkable_surfaces = ["green","desert"]
var moved
var map

func _ready():
	map = get_parent().map
	finish_turn()



func _process(delta):
	if moved == moves_per_turn:
		finished_turn = true


func pick_down():
	return $DownRay.get_collider()


func move(hex_):
	if walkable_surfaces.has(hex_.type):
		if hex_.has("soldier"):
			map.fight(self,hex_.get_from_hex("soldier"))
		else:
			position.x = hex_.position.x
			position.z = hex_.position.z
			moved += 1


func finish_turn():
	moved = 0
	finished_turn = false


func rmb_action():
	var buf = get_parent().pick_object()
	if map.get_near_hex(pick_down()).has(buf):
				var temp_ = buf
				move(temp_)
