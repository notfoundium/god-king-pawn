extends CSGMesh3D


var types
var type
var coords
var on_hex

func _ready():
	coords = Vector2.ZERO
	types = {}
	on_hex = []
	types["green"] = preload("res://resource/material/green.tres")
	types["desert"] = preload("res://resource/material/desert.tres")
	types["ghost"] = preload("res://resource/material/ghost.tres")
	change_type("ghost")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass


func change_type(str_):
	material = types[str_]
	type = str_


func get_from_hex(str_):
	var temp = null
	for i in on_hex:
		if i.scene_file_path.to_lower().contains(str_):
			temp = i
	return temp


func has(str_):
	if get_from_hex(str_) != null:
		return true
	else:
		return false


func change_coords(coords_):
	coords = coords_
	$Info.text = var_to_str(int(coords.x)) + "|" + var_to_str(int(coords.y))


func add_on_hex(object_):
	if not on_hex.has(object_):
		on_hex.append(object_)
		return 1
	else:
		return null


func remove_from_hex(object_):
	if on_hex.has(object_):
		on_hex.erase(object_)
		return 1
	else:
		return null
