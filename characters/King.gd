extends Node


var finished_turn
var settled
var hired

var soldier = preload("res://objects/Soldier.tscn")

var map

func _ready():
	map = get_parent().map


func _process(delta):
	if settled or hired:
		finished_turn = true


func finish_turn():
	finished_turn = false
	settled = false
	hired = false


func settle(building_,hex_):
	var bldng = map.settle(building_,hex_)
	if bldng != null:
		settled = true


func hire(who_, where_):
	var temp = get(who_).instantiate()
	map.create(temp, where_)
	hired = true


func rmb_action():
	var picked = get_parent().pick_object()
	if picked.scene_file_path == "res://objects/HexPlain.tscn":
		if picked.has("village"):
			hire("soldier", picked)
		else:
			settle("village", picked)
