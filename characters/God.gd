extends Node


var finished_turn
var changed_hex
var map

func _ready():
	map = get_parent().map
	finish_turn()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if changed_hex:
		finished_turn = true


func change_hex(hex_, type_):
	if not type_ == hex_.type:
		map.change_hex(hex_, type_)
		changed_hex = true


func finish_turn():
	finished_turn = false
	changed_hex = false


func rmb_action():
	change_hex(get_parent().pick_object(),"green")
